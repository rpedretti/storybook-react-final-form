/* eslint-disable react/display-name */
import * as React from 'react';
import addons, { makeDecorator } from '@storybook/addons';
import { Form, FormProps } from 'react-final-form';
import { FORM_ON_SUBMIT, FORM_SUBMIT, STATE_CHANGED } from './shared';

export const withReactFinalForm = makeDecorator({
    name: 'withReactFinalForm',
    parameterName: 'reactFinalForm',
    skipIfNoParametersOrOptions: false,
    wrapper: (getStory, context, { parameters }) => {
        const initialConfig = (context.loaded?.reactFinalForm ||
            parameters ||
            {}) as FormProps<any>;
        const channel = addons.getChannel();
        let submitter: () =>
            | Promise<Record<string, any> | undefined>
            | undefined;

        channel.on(FORM_SUBMIT, () => {
            submitter?.();
        });

        const { _onSubmit, ...rest } = initialConfig;

        return (
            <Form
                {...rest}
                onSubmit={(v) => {
                    channel.emit(FORM_ON_SUBMIT, v);
                }}
            >
                {(props) => {
                    if (!submitter) {
                        submitter = props.form.submit;
                    }
                    channel.emit(STATE_CHANGED, props.form.getState());
                    return getStory(context);
                }}
            </Form>
        );
    },
});
