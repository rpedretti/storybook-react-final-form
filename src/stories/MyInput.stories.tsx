import { Story } from '@storybook/react';
import * as React from 'react';
import MyInput from './MyInput';

type InputProps = React.ComponentProps<typeof MyInput>;

const Template: Story<InputProps> = (args) => (
    <>
        <img src="./assets/pudim.jpg" alt="pudim" width="150px" height="auto" />
        <MyInput {...args} />
    </>
);

export const Empty: Story<InputProps> = Template.bind({});
Empty.args = { name: 'myValue' };
Empty.parameters = {
    reactFinalForm: {
        initialValues: {},
    },
};

export const WithInitialValue: Story<InputProps> = Template.bind({});
WithInitialValue.storyName = 'With initial value';
WithInitialValue.args = { name: 'myValue' };
(WithInitialValue as any).loaders = [
    async () => {
        const p = new Promise((resolve) => {
            setTimeout(
                () =>
                    resolve({
                        reactFinalForm: {
                            initialValues: {
                                myValue: 'Hello',
                            },
                        },
                    }),
                2000,
            );
        });

        return p;
    },
];
WithInitialValue.parameters = {
    reactFinalForm: {},
};

export const WithValidation: Story<InputProps> = Template.bind({});
WithValidation.storyName = 'With Validation';
WithValidation.args = {
    name: 'myValue',
    validate: (v) => (v !== 'Hello' ? 'Wrong Value' : null),
};
WithValidation.parameters = {
    reactFinalForm: {
        initialValues: {
            myValue: 'Hello',
        },
    },
};
