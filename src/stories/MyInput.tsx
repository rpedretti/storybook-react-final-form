import { FieldState } from 'final-form';
import * as React from 'react';
import { Field } from 'react-final-form';

interface MyInputProps {
    name: string;
    validate?: (
        value: any,
        allValues: Record<string, unknown>,
        meta?: FieldState<any>,
    ) => any | Promise<any>;
}

const MyInput = (props: MyInputProps) => (
    <Field name={props.name} validate={props.validate}>
        {({ input, meta }) => (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <input {...input} type="text" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
            </div>
        )}
    </Field>
);

export default React.memo(MyInput);
