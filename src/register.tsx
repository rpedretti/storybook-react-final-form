/* eslint-disable react/display-name */
import React from 'react';
import addons from '@storybook/addons';

import { AddonPanel } from '@storybook/components';

import Panel from './components/Panel';

import { REACT_FINAL_FORM, REACT_FINAL_FORM_PANNEL } from './contants';

addons.register(REACT_FINAL_FORM, () => {
    addons.addPanel(REACT_FINAL_FORM_PANNEL, {
        title: 'Final Form',
        paramKey: 'reactFinalForm',
        render: ({ active, key }) => {
            console.debug(key);
            return (
                <AddonPanel key={key} active={active || false}>
                    <Panel />
                </AddonPanel>
            );
        },
    });
});
