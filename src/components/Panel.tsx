import { useChannel } from '@storybook/api';
import { FormState } from 'final-form';
import * as React from 'react';
import { FORM_SUBMIT, STATE_CHANGED } from '../shared';

const parentStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
};

const containerStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
};

const itemsStyle: React.CSSProperties = {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: '0px',
    borderLeft: '1px solid red',
    borderRight: '1px solid red',
    padding: '4px 8px',
};

const Panel = () => {
    const [formState, setFormState] = React.useState<FormState<any>>();

    const emit = useChannel({
        [STATE_CHANGED]: async (state: FormState<any>) => {
            setFormState(state);
        },
    });

    return (
        <div style={parentStyle}>
            <div>
                <button onClick={() => emit(FORM_SUBMIT)}>Submit</button>
            </div>
            <div style={containerStyle}>
                <div style={itemsStyle}>
                    <h1>Values</h1>
                    <pre>{JSON.stringify(formState?.values, null, 2)}</pre>
                </div>
                <div style={itemsStyle}>
                    <h1>Touched</h1>
                    <pre>{JSON.stringify(formState?.touched, null, 2)}</pre>
                </div>
                <div style={itemsStyle}>
                    <h1>Errors</h1>
                    <pre>{JSON.stringify(formState?.errors, null, 2)}</pre>
                </div>
            </div>
        </div>
    );
};

export default Panel;
