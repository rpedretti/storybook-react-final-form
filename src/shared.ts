export const FORM_SUBMIT = 'react-final-form-submit';
export const FORM_ON_SUBMIT = 'react-final-form-on-submit';
export const STATE_CHANGED = 'react-final-form-state-changed';