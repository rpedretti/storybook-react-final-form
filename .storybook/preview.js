import { withReactFinalForm } from '../src/withReactFinalForm';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

export const decorators = [
  withReactFinalForm
]